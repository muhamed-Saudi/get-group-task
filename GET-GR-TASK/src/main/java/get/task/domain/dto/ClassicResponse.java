package get.task.domain.dto;

import java.io.Serializable;

public class ClassicResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;

	private int resultCode;

	private String message;

	public ClassicResponse() {
		super();
	}

	public ClassicResponse(int resultCode, String message) {
		super();
		this.resultCode = resultCode;
		this.message = message;
	}

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id=id;
	}

}
