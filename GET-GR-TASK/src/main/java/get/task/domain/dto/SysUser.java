package get.task.domain.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SysUser implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userId;
	private String email;
	private String userLoginName;
	private String password;
	private String name;
	private String userRoleCode;
	
	public SysUser() {}

	public SysUser(String userId, String email, String userLoginName, String password, String name,
			String userRoleCode) {
		super();
		this.userId = userId;
		this.email = email;
		this.userLoginName = userLoginName;
		this.password = password;
		this.name = name;
		this.userRoleCode = userRoleCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserLoginName() {
		return userLoginName;
	}

	public void setUserLoginName(String userLoginName) {
		this.userLoginName = userLoginName;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserRoleCode() {
		return userRoleCode;
	}

	public void setUserRoleCode(String userRoleCode) {
		this.userRoleCode = userRoleCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result + ((userLoginName == null) ? 0 : userLoginName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SysUser other = (SysUser) obj;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		if (userLoginName == null) {
			if (other.userLoginName != null)
				return false;
		} else if (!userLoginName.equals(other.userLoginName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SysUser [userId=" + userId + ", email=" + email + ", userLoginName=" + userLoginName + ", name=" + name
				+ ", userRoleCode=" + userRoleCode + "]";
	}

}
