package get.task.domain.dbrepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import get.task.domain.model.PendingStudent;
import java.lang.String;
import java.util.List;

@Repository
public interface PendingStudentRepository extends JpaRepository<PendingStudent, String>{

	List<PendingStudent> findByStatus(String status);
}
