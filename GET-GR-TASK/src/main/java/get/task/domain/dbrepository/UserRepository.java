package get.task.domain.dbrepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import get.task.domain.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String>{

	List<User> findByUserRoleCode(String userrolecode);
	
	Optional<User> findByUsername(String username);
}
