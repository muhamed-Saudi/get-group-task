package get.task.service.contract;

import java.util.List;
import java.util.Optional;

import get.task.domain.dto.SysUser;
import get.task.domain.model.PendingStudent;
import get.task.util.constant.SystemConstantEnum;

public interface PendingStudentService {

	boolean createNewPendingStudent(SysUser studentUser);

	Optional<PendingStudent> getPendingStudentById(String pendingStudentId);

	boolean acceptPendingStudent(String pendingStudentId);

	boolean rejectPendingStudent(String pendingStudentId);

	List<SysUser> getAllStudentByStatus(SystemConstantEnum status);

}