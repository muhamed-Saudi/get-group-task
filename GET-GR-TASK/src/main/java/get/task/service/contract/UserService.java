package get.task.service.contract;

import java.util.List;

import get.task.domain.dto.SysUser;
import get.task.domain.model.PendingStudent;

public interface UserService {

	boolean createNewStudent(PendingStudent pendingStudent);
	List<SysUser> getAllStudent();
	SysUser getByUsername(String username);
}
