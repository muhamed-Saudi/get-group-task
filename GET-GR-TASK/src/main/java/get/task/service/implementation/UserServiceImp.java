package get.task.service.implementation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import get.task.domain.dbrepository.PendingStudentRepository;
import get.task.domain.dbrepository.UserRepository;
import get.task.domain.dto.SysUser;
import get.task.domain.model.PendingStudent;
import get.task.domain.model.User;
import get.task.service.contract.UserService;
import get.task.util.Util;
import get.task.util.constant.UserRoleEnum;

@Service
@Transactional
public class UserServiceImp implements UserService{

	
	@Autowired
	Util util;
	@Autowired
	PendingStudentRepository pendingRequestRepository;
	@Autowired
	UserRepository userRepository;
	
	@Override
	public boolean createNewStudent(PendingStudent pendingStudent) {
		User user=util.getUserEntity(pendingStudent);
		userRepository.save(user);
		return true;
	}
	
	@Override
	public List<SysUser> getAllStudent(){
		
		List<User> users=userRepository.findByUserRoleCode(UserRoleEnum.STUDENT_USER.getRoleCode());
		if(users !=null && !users.isEmpty()) {
			
			List<SysUser> allUsers=new ArrayList<>();
			users.forEach(user->{
				allUsers.add(util.getSysUserDTO(user));
			});
			return allUsers;
		}
		return Collections.emptyList();
		
	}
	
	@Override
	public SysUser getByUsername(String username) {
		
		Optional<User> optionalUser=userRepository.findByUsername(username);
		if(optionalUser.isPresent()) {
			return util.getSysUserDTO(optionalUser.get());
		}
		return null;
	}



}
