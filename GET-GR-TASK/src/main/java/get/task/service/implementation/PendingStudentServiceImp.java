package get.task.service.implementation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import get.task.domain.dbrepository.PendingStudentRepository;
import get.task.domain.dto.SysUser;
import get.task.domain.model.PendingStudent;
import get.task.exception.InvalidValueException;
import get.task.service.contract.PendingStudentService;
import get.task.service.contract.UserService;
import get.task.util.Util;
import get.task.util.constant.SystemConstantEnum;

@Service
@Transactional
public class PendingStudentServiceImp implements PendingStudentService {

	@Autowired
	Util util;
	@Autowired
	PendingStudentRepository pendingStudentRepository;
	@Autowired
	UserService userService;
	
	
	@Override
	public boolean createNewPendingStudent(SysUser studentUser) {
		
		if(!util.isValid(studentUser)) {
			throw new InvalidValueException("UserServiceImp::createNewStudent: Given Student Can't Be created, please review the mandatory fields..");
		}
		
		PendingStudent pendingStudent=util.getPendingStudentEntity(studentUser);
		pendingStudent.setStatus(SystemConstantEnum.PENDING_STATUS.getName());
		pendingStudentRepository.save(pendingStudent);
		return true;
	}
	
	@Override
	public List<SysUser> getAllStudentByStatus(SystemConstantEnum status){
		
		 List<PendingStudent> allPendingStudents=pendingStudentRepository.findByStatus(status.getName());
		 
		 if(allPendingStudents !=null && !allPendingStudents.isEmpty()) {	 
			 List<SysUser> pendingStudentsDTO=new ArrayList<>();
			 allPendingStudents.forEach(pendingStudent->{
				 	pendingStudentsDTO.add(util.getSysUserDTO(pendingStudent));
			 });
			 return pendingStudentsDTO;
		 }
		
		return Collections.emptyList();
	}
	
	@Override
	public Optional<PendingStudent> getPendingStudentById(String pendingStudentId) {
		
		return Optional.ofNullable(pendingStudentRepository.findById(pendingStudentId)).orElse(null);	
	}
	
	@Override
	public boolean acceptPendingStudent(String pendingStudentId) {
		
		Optional<PendingStudent> pendingStudentOptional= getPendingStudentById(pendingStudentId);
		if(pendingStudentOptional.isPresent()) {
			
			PendingStudent pendingStudent=pendingStudentOptional.get();
			boolean isSaved=userService.createNewStudent(pendingStudent);
			if(!isSaved)
				return false;
			
			pendingStudentRepository.delete(pendingStudent);
			return true;
		}
		
		throw new InvalidValueException(String.format("PendingStudentServiceImp::acceptPendingStudent: Can't Find Pending Student With ID:[%s]",pendingStudentId));
	}
	
	@Override
	public boolean rejectPendingStudent(String pendingStudentId) {
		
		Optional<PendingStudent> pendingStudentOptional= getPendingStudentById(pendingStudentId);
		if(pendingStudentOptional.isPresent()) {
			PendingStudent pendingStudent=pendingStudentOptional.get();
			pendingStudent.setStatus(SystemConstantEnum.REJECTED_STATUS.getName());
			pendingStudentRepository.save(pendingStudent);
			return true;
		}
		
		throw new InvalidValueException(String.format("PendingStudentServiceImp::rejectPendingStudent: Can't Find Pending Student With ID:[%s]",pendingStudentId));
	}
}
