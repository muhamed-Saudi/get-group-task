package get.task.util.constant;

public enum UserRoleEnum {

	STAFF_USER("1001","STAFF"),
	HR_USRE("1002", "HR"),
	STUDENT_USER("1003", "STUDENT");
	
	private String roleCode;
	private String roleName;

	private UserRoleEnum(String roleCode, String roleName) {
		this.roleName = roleName;
		this.roleCode=roleCode;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	
	
}
