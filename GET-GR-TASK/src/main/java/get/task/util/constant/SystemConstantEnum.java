package get.task.util.constant;

public enum SystemConstantEnum {

	REJECTED_STATUS ("Rejected"),
	PENDING_STATUS("Pending");
	
	private String name;
	
	private SystemConstantEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
