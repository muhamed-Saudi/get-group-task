package get.task.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import get.task.domain.dto.SysUser;
import get.task.domain.model.PendingStudent;
import get.task.domain.model.User;
import get.task.exception.InvalidValueException;
import get.task.util.constant.UserRoleEnum;


@Configuration
@PropertySource("classpath:application.properties")
public class Util {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	public boolean isVoid(String str) {
		return (str == null) || str.isEmpty();
	}
	
	public boolean isVoid(StringBuilder str) {
		return (str == null) || str.toString().isEmpty();
	}

	public boolean isVoid(List<String> list) {
		return (list == null) || list.isEmpty();
	}
	
	public String encodeValue(String value) {
	    String encodedValue=null;
		try {
			encodedValue = URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException e) {
			log.error("Error While Trying To Encode Ouath2 URL: "+e.getMessage());
			throw new InvalidValueException("Error While Trying To Encode Ouath2 URL: "+e.getMessage());
		}
	    return encodedValue;
	}
	
	public String decode(String value) {
		 String deCcodedValue=null;
			try {
				deCcodedValue = URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
			} catch (UnsupportedEncodingException e) {
				//log.error("Error While Trying To Encode Ouath2 URL: "+e.getMessage());
				//throw new InvalidValueException("Error While Trying To Encode Ouath2 URL: "+e.getMessage());
			}
		    return deCcodedValue;
	}	
	
	public String hashPassword(String plainTextPassword){
		return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
	}
	
	public User getUserEntity(PendingStudent pendingStudentUser) {
		
		User user=new User();
		user.setFullName(pendingStudentUser.getFullName());
		user.setPassword(pendingStudentUser.getPassword());
		user.setUsername(pendingStudentUser.getUsername());
		user.setUserEmail(pendingStudentUser.getUserEmail());
		user.setUserRoleCode(UserRoleEnum.STUDENT_USER.getRoleCode());
		return user;
	}
	
	public PendingStudent getPendingStudentEntity(SysUser studentUser) {
		
		PendingStudent pendingStudent=new PendingStudent();
		pendingStudent.setFullName(studentUser.getName());
		pendingStudent.setUserEmail(studentUser.getEmail());
		pendingStudent.setUsername(studentUser.getUserLoginName());
		/* Hash Password First Before DB Persistaing*/
		String hashedPassword= hashPassword(studentUser.getPassword());
		pendingStudent.setPassword(hashedPassword);
		
		return pendingStudent;
	}
	
	public SysUser getSysUserDTO(PendingStudent pendingStudent) {
		
		SysUser sysUser=new SysUser();
		sysUser.setName(pendingStudent.getFullName());
		sysUser.setUserLoginName(pendingStudent.getUsername());
		sysUser.setPassword(pendingStudent.getPassword());
		sysUser.setEmail(pendingStudent.getUserEmail());
		sysUser.setUserId(pendingStudent.getPendingRequestId());
		sysUser.setUserRoleCode(UserRoleEnum.STUDENT_USER.getRoleCode());
		return sysUser;
	}
	
	public SysUser getSysUserDTO(User user) {
		
		SysUser sysUser=new SysUser();
		sysUser.setUserId(user.getUserId());
		sysUser.setName(user.getFullName());
		sysUser.setUserLoginName(user.getUsername());
		sysUser.setPassword(user.getPassword());
		sysUser.setEmail(user.getUserEmail());
		sysUser.setUserRoleCode(UserRoleEnum.STUDENT_USER.getRoleCode());
		return sysUser;
	}
	
	public boolean isValid(SysUser studentUser) {
		
		if(studentUser == null)
			return false;
		
		if(isVoid(studentUser.getEmail()) || isVoid(studentUser.getUserLoginName()) || isVoid(studentUser.getPassword()))
			return false;
		
		return true;
	}
}