package get.task.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import get.task.util.constant.UserRoleEnum;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	 private static String REALM="MY_TEST_REALM";
	 @Autowired
	 private PasswordEncoder passwordEncoder;
	private static final String[] AUTH_WHITELIST = {
            // -- Swagger
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/get-task/swagger-ui.html",
            "/webjars/**",
            "/api/pending-student/create"
    };

	@Override
	protected void configure(HttpSecurity http) throws Exception {
			http
			.authorizeRequests()
			//Enable unauthorized user to consume token generator API
			.antMatchers(AUTH_WHITELIST).permitAll() 
			// Disable consuming any API except the authorized users
			.and().authorizeRequests().anyRequest().authenticated()
			.and().httpBasic().realmName(REALM).authenticationEntryPoint(getBasicAuthEntryPoint())
			.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
			.and().csrf().disable();
			
	}
	
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().passwordEncoder(passwordEncoder()).withUser("st-user").password(passwordEncoder.encode("abc@123")).roles(UserRoleEnum.STAFF_USER.getRoleName());
        auth.inMemoryAuthentication().passwordEncoder(passwordEncoder()).withUser("hr-user").password(passwordEncoder.encode("abc@123")).roles(UserRoleEnum.HR_USRE.getRoleName());
        
    }
    
    @Bean
    public CustomBasicAuthenticationEntryPoint getBasicAuthEntryPoint(){
        return new CustomBasicAuthenticationEntryPoint();
    }
	
	 @Override
	 public void configure(WebSecurity web) throws Exception {
	        web.ignoring().antMatchers(AUTH_WHITELIST);
	 }
	
	 @Bean
	 public PasswordEncoder passwordEncoder() {
	      return new BCryptPasswordEncoder();
	 }
}