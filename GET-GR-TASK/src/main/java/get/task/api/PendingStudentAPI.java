package get.task.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import get.task.domain.dto.ClassicResponse;
import get.task.domain.dto.SysUser;
import get.task.service.contract.PendingStudentService;
import get.task.util.constant.SystemConstantEnum;

@RestController
@RequestMapping("/api/pending-student")
public class PendingStudentAPI {

	@Autowired
	PendingStudentService pendingStudentService;
	
	@PostMapping("/create")
	public ResponseEntity<Object> createNewStudent(@RequestBody(required=true) SysUser sysUser){
		
		ClassicResponse classicResponse=new ClassicResponse(200, "Student Creation Request Sent Successfully, pleaase Wait For Approval ..");
		try {
			boolean isSaved=pendingStudentService.createNewPendingStudent(sysUser);
			if(!isSaved) {
				classicResponse.setMessage("Can't Save Student..");
				return new ResponseEntity<>(classicResponse, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<>(classicResponse, HttpStatus.OK);
		}catch(Exception ex) {
			classicResponse.setResultCode(500);
			classicResponse.setMessage(String.format("Can't Create New Student Request With Error Message:[%s] and Exception With Type:[%s]",ex.getMessage(),ex.getClass()));
			return new ResponseEntity<>(classicResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	@GetMapping(path="/get-pending-students", produces = "application/json")
	@PreAuthorize("hasPermission(returnObject, 'HR')")
	public ResponseEntity<Object> getAllPendingStudent(){
		
		ClassicResponse classicResponse=new ClassicResponse();
		try {
			List<SysUser> users=pendingStudentService.getAllStudentByStatus(SystemConstantEnum.PENDING_STATUS);
			return new ResponseEntity<>(users, HttpStatus.OK);
		}catch(Exception ex) {
			classicResponse.setResultCode(500);
			classicResponse.setMessage(String.format("Can't Get Pending Students With Error Message:[%s] and Exception With Type:[%s]",ex.getMessage(),ex.getClass()));
			return new ResponseEntity<>(classicResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	@GetMapping(path="/get-rejected-students", produces = "application/json")
	@PreAuthorize("hasPermission(returnObject, 'HR')")
	public ResponseEntity<Object> getAllRejectedStudent(){
		
		ClassicResponse classicResponse=new ClassicResponse();
		try {
			List<SysUser> users=pendingStudentService.getAllStudentByStatus(SystemConstantEnum.REJECTED_STATUS);
			return new ResponseEntity<>(users, HttpStatus.OK);
		}catch(Exception ex) {
			classicResponse.setResultCode(500);
			classicResponse.setMessage(String.format("Can't Get Rejected Students With Error Message:[%s] and Exception With Type:[%s]",ex.getMessage(),ex.getClass()));
			return new ResponseEntity<>(classicResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	@GetMapping("/accept")
	@PreAuthorize("hasPermission(returnObject, 'HR')")
	public ResponseEntity<Object> acceptStudentRequest(@RequestParam(name="pendingStudentId",required=true) String pendingStudentId){
		
		ClassicResponse classicResponse=new ClassicResponse(200, "Student Request has been accepted Successfully");
		try {
			boolean isAccepted=pendingStudentService.acceptPendingStudent(pendingStudentId);
			if(!isAccepted) {
				classicResponse.setMessage("Can't Accept Student..");
				return new ResponseEntity<>(classicResponse, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<>(classicResponse, HttpStatus.OK);
		}catch(Exception ex) {
			classicResponse.setResultCode(500);
			classicResponse.setMessage(String.format("Can't Accept Student Request With Error Message:[%s] and Exception With Type:[%s]",ex.getMessage(),ex.getClass()));
			return new ResponseEntity<>(classicResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	@GetMapping("/reject")
	@PreAuthorize("hasPermission(returnObject, 'HR')")
	public ResponseEntity<Object> rejectStudentRequest(@RequestParam(name="pendingStudentId",required=true) String pendingStudentId){
		
		ClassicResponse classicResponse=new ClassicResponse(200, "Student Request has been Rejected Successfully");
		try {
			boolean isRejected=pendingStudentService.rejectPendingStudent(pendingStudentId);
			if(!isRejected) {
				classicResponse.setMessage("Can't Reject Student..");
				return new ResponseEntity<>(classicResponse, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<>(classicResponse, HttpStatus.OK);
		}catch(Exception ex) {
			classicResponse.setResultCode(500);
			classicResponse.setMessage(String.format("Can't Reject Student Request With Error Message:[%s] and Exception With Type:[%s]",ex.getMessage(),ex.getClass()));
			return new ResponseEntity<>(classicResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
}