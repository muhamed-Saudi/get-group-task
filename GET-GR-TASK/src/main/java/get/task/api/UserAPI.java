package get.task.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import get.task.domain.dto.ClassicResponse;
import get.task.domain.dto.SysUser;
import get.task.service.contract.PendingStudentService;
import get.task.service.contract.UserService;

@RestController
@RequestMapping("/api/user")
public class UserAPI {

	@Autowired
	UserService userService;
	@Autowired
	PendingStudentService pendingStudentService;
	
	@GetMapping(path="/get-all", produces = "application/json")
	@PreAuthorize("hasPermission(returnObject, 'STAFF')")
	public ResponseEntity<Object> getAllStudent(){
		
		ClassicResponse classicResponse=new ClassicResponse();
		try {
			List<SysUser> users=userService.getAllStudent();
			return new ResponseEntity<>(users, HttpStatus.OK);
		}catch(Exception ex) {
			classicResponse.setResultCode(500);
			classicResponse.setMessage(String.format("Can't Get Students With Error Message:[%s] and Exception With Type:[%s]",ex.getMessage(),ex.getClass()));
			return new ResponseEntity<>(classicResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	@GetMapping(path="/get-all", produces = "application/json")
	public ResponseEntity<Object> getAll22(){
		
		ClassicResponse classicResponse=new ClassicResponse();
		try {
			List<SysUser> users=userService.getAllStudent();
			return new ResponseEntity<>(users, HttpStatus.OK);
		}catch(Exception ex) {
			classicResponse.setResultCode(500);
			classicResponse.setMessage(String.format("Can't Get Students With Error Message:[%s] and Exception With Type:[%s]",ex.getMessage(),ex.getClass()));
			return new ResponseEntity<>(classicResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
}
