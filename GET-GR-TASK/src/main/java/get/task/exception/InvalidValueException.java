package get.task.exception;

public class InvalidValueException extends RuntimeException {

    private static final long serialVersionUID = 4570573798200354363L;

    public InvalidValueException(String msg) {
        super(msg);
    }

    public InvalidValueException(Throwable cause) {
        super(cause);
    }

    public InvalidValueException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
