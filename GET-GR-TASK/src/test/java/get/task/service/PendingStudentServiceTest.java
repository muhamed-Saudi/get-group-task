package get.task.service;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import get.task.GetTaskApp;
import get.task.domain.dbrepository.PendingStudentRepository;
import get.task.domain.dto.SysUser;
import get.task.domain.model.PendingStudent;
import get.task.service.contract.PendingStudentService;
import get.task.util.constant.UserRoleEnum;

@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ContextConfiguration(classes = {GetTaskApp.class})
public class PendingStudentServiceTest {

	@Autowired
	PendingStudentService pendingStudentService;
	@Autowired
	PendingStudentRepository repo;


	@Before
	@Transactional
	public void setUp() {
		
		SysUser studentUser=new SysUser("100", "msaudi@gmail.com", "Ahmed", "123", "Ahmed", UserRoleEnum.STUDENT_USER.getRoleCode());
		boolean isCreated=pendingStudentService.createNewPendingStudent(studentUser);
		assertTrue("failure - should be true", isCreated);
	}
	
	@After
	@Transactional
	public void destroy() {

		repo.deleteById("100");
	}
	
	@Test
	@Transactional
	public void canCreate() {
		
		SysUser studentUser=new SysUser("200", "msaudi@outlook.com", "Muhamed", "567", "Muhamed", UserRoleEnum.STUDENT_USER.getRoleCode());
		boolean isCreated=pendingStudentService.createNewPendingStudent(studentUser);
		assertTrue("failure - should be true", isCreated);
		
	}
	
	@Test
	public void canFindRequest() {
		
		Optional<PendingStudent> optional=pendingStudentService.getPendingStudentById("100");
		assertTrue("failure - should be true", optional.isPresent());
		
	}
	
}