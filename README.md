# GET-Group Task

- This Repository Contains Spring boot Project
                               
	2- GET-GR-task:    This Project Contains all Functionality needed and it exposes it as a REST-API services.					   (http://localhost:9002/get-task/api)                          
	
- Technologies Used:-                                                                                                                                      
1- Spring boot 2.0.3                                                                                                                                                    
2- Spring Data Jpa                                                                                                                                      
3- Spring Security                                                                                                                                                                                                                                                                                
5- H2 In-memory Database                                                                                                                                             
6- Swagger For API Documentations    
7- Junit, restassured and Mockito For Testing Purposes.                                                                                                                                                                                                                                             
	
Note: It will be Great to have POSTMAN Installed to import Collection from here "https://www.getpostman.com/collections/6ba7ef9d62d6ae65b350"                                                               
                                                                    

- Available Users Credentials:-                                                                                                                       
A- st-user/abc@123  with STAFF Priviliges                                                                                                                    
B- hr-user/abc@123  with HR Priviliges                                                                                                                                                                                                                                                                                                                            

4- You can consume any available API and you can check them at                       
"http://localhost:9002/get-task/swagger-ui.html".                                                                     









- 
